const express = require("express");
const User = require("../model/user");
const router = new express.Router();
const auth = require("../../middleware/auth");
const multer = require("multer");

//add User
router.post("/", async (req, res) => {
  const user = new User(req.body);
  try {
    await user.save();
    const token = await user.generateAuthToken();
    res.status(201).send({ user, token });
  } catch (e) {
    console.log(e);
    res.status(400).send("duplicate email");
  }
});

//logout
router.post("/logout", auth, async (req, res) => {
  try {
    req.user.tokens = req.user.tokens.filter(
      (token) => token.token !== req.token
    );
    await req.user.save();
    res.send("logout");
  } catch (e) {
    console.log(e);
    res.status(500).send();
  }
});

router.post("/logoutAll", auth, async (req, res) => {
  try {
    req.user.tokens = [];
    await req.user.save();
    res.send("logout all");
  } catch (e) {
    console.log(e);
    res.status(500).send();
  }
});

//getAllUser
router.get("/", auth, async (req, res) => {
  res.send(req.user);
});

//getUserbyId
router.get("/:id", async (req, res) => {
  const _id = req.params.id;
  try {
    const user = await User.findById(_id);
    if (!user) {
      return res.status(404).send();
    }
    res.send(user);
  } catch (e) {
    res.status(500).send();
  }
});

//login
router.post("/login", async (req, res) => {
  try {
    const user = await User.findByCredentials(
      req.body.email,
      req.body.password
    );
    const token = await user.generateAuthToken();
    res.status(200).send({ user, token });
  } catch (e) {
    console.log(e);
    res.status(401).send("Incorrect username or password");
  }
});

//update User
router.put("/", auth, async (req, res) => {
  const updates = Object.keys(req.body);
  try {
    const user = req.user;
    updates.forEach((update) => (user[update] = req.body[update]));
    await user.save();
    if (!user) {
      return res.status(404).send();
    }
    res.send(user);
  } catch (e) {
    console.log(e);
    res.status(400).send(e);
  }
});

//delete user
router.delete("/", auth, async (req, res) => {
  try {
    await req.user.remove();
    res.send(req.user);
  } catch (e) {
    res.status(500).send();
  }
});

const upload = multer({
  limits: {
    fileSize: 10000000,
  },
  fileFilter(req, file, cb) {
    if (!file.originalname.match(/\.(jpg|jpeg|png|PNG|JPEG|JPG)$/)) {
      return cb(new Error("Please upload an Image!"));
    }
    cb(undefined, true);
  },
});

router.post(
  "/avatar",
  auth,
  upload.single("upload"),
  async (req, res) => {
    /*
      uplod to wherever you want
    */
    await req.user.save();
    res.send();
  },
  (error, req, res, next) => {
    res.status(400).send({ error: error.message });
  }
);

module.exports = router;
