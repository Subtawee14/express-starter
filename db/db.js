const mongoose = require("mongoose");
mongoose
  .connect(`${process.env.MONGO_URL}/test`, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log("DB connected!!"))
  .catch((error) => console.log("can't connect to DB : ", error));

mongoose.connection.on("error", (err) => {
  console.log(err);
});
